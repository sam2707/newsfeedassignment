package com.newfeed.newsfeedapplication.ui

import android.R
import android.support.test.rule.ActivityTestRule
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.newfeed.newsfeedapplication.main.ui.NewsFeedHomeActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class NewsFeedHomeActivityInstrumentationTest {

    // Preferred JUnit 4 mechanism of specifying the activity to be launched before each test
    @Rule
    var activityTestRule: ActivityTestRule<NewsFeedHomeActivity> =
        ActivityTestRule(NewsFeedHomeActivity::class.java)

    // Looks for an EditText with id = "R.id.etInput"
    // Types the text "Hello" into the EditText
    // Verifies the EditText has text "Hello"
    @Test
    fun validateProgressBar() {
        onView(withId(R.id.progress)).check(matches(isDisplayed()))
    }
}