package com.newfeed.newsfeedapplication.viewmodel

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.newfeed.newsfeedapplication.main.network.ApiClient
import com.newfeed.newsfeedapplication.main.network.RemoteDataSource
import com.newfeed.newsfeedapplication.main.repository.AppRepository
import com.newfeed.newsfeedapplication.main.viewmodel.NewsFeedViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.*
import org.junit.*
import org.junit.rules.TestRule
import org.mockito.Mockito.mock 

class NewsFeedViewModelTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    private val mDispatcher = TestCoroutineDispatcher()
    private val testScope = TestCoroutineScope(mDispatcher)


    @Test
    fun getFeedsFromAPIFailure() {
        val apiClient = mock(ApiClient::class.java)
        val application = mock(Application::class.java)
        val remoteDataSource = RemoteDataSource(apiClient)
        val ifSuccess = NewsFeedViewModel(AppRepository(remoteDataSource), application)
            .getFeedsFromNetwork().isCompleted
        Assert.assertEquals(false, ifSuccess)
    }

    @Test
    fun testAPI() = testScope.runBlockingTest {
        val apiClient = mock(ApiClient::class.java)
        val application = mock(Application::class.java)
        val remoteDataSource = RemoteDataSource(apiClient)
        NewsFeedViewModel(AppRepository(remoteDataSource), application).getFeedsFromNetwork()
    }

    @Before
    fun setup() {
        Dispatchers.setMain(mDispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }
}