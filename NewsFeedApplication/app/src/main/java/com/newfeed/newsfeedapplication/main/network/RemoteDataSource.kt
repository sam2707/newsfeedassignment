package com.newfeed.newsfeedapplication.main.network

import javax.inject.Inject

class RemoteDataSource @Inject constructor(private val apiClient: ApiClient) {
    suspend fun getFactsFromAPI() = apiClient.getFeedsFromAPI()
}