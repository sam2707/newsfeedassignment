package com.newfeed.newsfeedapplication.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.newfeed.newsfeedapplication.R
import com.newfeed.newsfeedapplication.main.model.Rows

class NewsFeedAdapter(private var feeds: List<Rows>? = null) :
    RecyclerView.Adapter<NewsFeedAdapter.NewsFeedViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsFeedViewHolder {
        return NewsFeedViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.feed_item_layout, parent, false)
        )
    }

    inner class NewsFeedViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val name: TextView = v.findViewById(R.id.feed_title)
        val status: TextView = v.findViewById(R.id.feed_description)
        val image: AppCompatImageView = v.findViewById(R.id.feed_image)
    }

    override fun getItemCount(): Int {
        return feeds?.size ?: 0
    }

    fun setData(list: List<Rows>) {
        this.feeds = list
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: NewsFeedViewHolder, position: Int) {
        val data = feeds?.get(position)
        with(holder) {
            name.text = data?.title
            status.text = data?.description
            image.load(data?.imageHref) {
                placeholder(R.drawable.ic_launcher_background)
                crossfade(true)
            }
        }
    }
}
