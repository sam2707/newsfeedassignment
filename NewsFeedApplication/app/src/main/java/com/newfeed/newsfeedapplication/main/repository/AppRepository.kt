package com.newfeed.newsfeedapplication.main.repository

import com.newfeed.newsfeedapplication.main.network.BaseApiResponse
import com.newfeed.newsfeedapplication.main.network.NetworkResult
import com.newfeed.newsfeedapplication.main.network.RemoteDataSource
import com.newfeed.newsfeedapplication.main.model.Feed
import dagger.hilt.android.scopes.ActivityRetainedScoped
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

@ActivityRetainedScoped
class AppRepository @Inject constructor(private val networkCall: RemoteDataSource) :
    BaseApiResponse() {
    // get response from api
    suspend fun getFeedsFromAPI(): Flow<NetworkResult<Feed>> {
        return flow {
            emit(NetworkResult.Loading())
            emit(safeApiCall { networkCall.getFactsFromAPI() })
        }.flowOn(Dispatchers.IO)
    }
}
