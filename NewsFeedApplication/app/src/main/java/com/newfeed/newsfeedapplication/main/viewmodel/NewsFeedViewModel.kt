package com.newfeed.newsfeedapplication.main.viewmodel

import android.app.Application
import androidx.lifecycle.*
import com.newfeed.newsfeedapplication.main.network.NetworkResult
import com.newfeed.newsfeedapplication.main.repository.AppRepository
import com.newfeed.newsfeedapplication.main.model.Feed
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NewsFeedViewModel @Inject constructor(
    private val repository: AppRepository,
    application: Application
) : AndroidViewModel(application) {

    private val mFactsResponse: MutableLiveData<NetworkResult<Feed>> = MutableLiveData()
    val mFacts: LiveData<NetworkResult<Feed>> = mFactsResponse

    // call the api when the viewmodel is initialized
    init {
        getFeedsFromNetwork()
    }

    /**
     * @desc fetch the data from the network using the api calls
     */
    fun getFeedsFromNetwork() = viewModelScope.launch {
        repository.getFeedsFromAPI().collect {
            mFactsResponse.value = it
        }
    }
}