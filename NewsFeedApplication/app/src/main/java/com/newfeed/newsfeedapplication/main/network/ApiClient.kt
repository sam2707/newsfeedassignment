package com.newfeed.newsfeedapplication.main.network

import com.newfeed.newsfeedapplication.main.model.Feed
import retrofit2.Response
import retrofit2.http.GET

interface ApiClient {
    @GET("facts.json")
    suspend fun getFeedsFromAPI() : Response<Feed>
}