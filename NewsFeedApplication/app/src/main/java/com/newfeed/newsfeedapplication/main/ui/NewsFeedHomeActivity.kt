package com.newfeed.newsfeedapplication.main.ui

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.snackbar.BaseTransientBottomBar.LENGTH_LONG
import com.google.android.material.snackbar.BaseTransientBottomBar.LENGTH_SHORT
import com.google.android.material.snackbar.Snackbar
import com.newfeed.newsfeedapplication.R
import com.newfeed.newsfeedapplication.databinding.ActivityNewsFeedBinding
import com.newfeed.newsfeedapplication.main.adapter.NewsFeedAdapter
import com.newfeed.newsfeedapplication.main.model.Feed
import com.newfeed.newsfeedapplication.main.model.Rows
import com.newfeed.newsfeedapplication.main.network.NetworkResult
import com.newfeed.newsfeedapplication.main.viewmodel.NewsFeedViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_news_feed.*

@AndroidEntryPoint
class NewsFeedHomeActivity : AppCompatActivity() {
    private lateinit var mActivityNewsFeedBinding: ActivityNewsFeedBinding
    private val mNewsFeedViewModel: NewsFeedViewModel by viewModels()
    private lateinit var mNewsFeedAdapter: NewsFeedAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivityNewsFeedBinding = DataBindingUtil.setContentView(
            this, R.layout.activity_news_feed
        )
        setSupportActionBar(mActivityNewsFeedBinding.appToolbar)
        initListAdapter()
        mActivityNewsFeedBinding.swipeRefreshLayout.setOnRefreshListener(refreshListener);
    }

    private val refreshListener = SwipeRefreshLayout.OnRefreshListener {
        mActivityNewsFeedBinding.swipeRefreshLayout.isRefreshing = true
        mNewsFeedAdapter.setData(ArrayList())
        // call api to reload data
        mNewsFeedViewModel.getFeedsFromNetwork()
    }

    //Initialize adapter
    private fun initListAdapter() {
        mNewsFeedAdapter = NewsFeedAdapter(null)
        mActivityNewsFeedBinding.feedList.apply {
            this.layoutManager = LinearLayoutManager(this@NewsFeedHomeActivity)
            this.adapter = mNewsFeedAdapter
        }
        // add observer for response
        mNewsFeedViewModel.mFacts.observe(this) { response ->
            run {
                initAdapter(response)
            }
        }
    }

    /**
     * @param response the response received from the network call
     */
    private fun initAdapter(response: NetworkResult<Feed>) {
        mActivityNewsFeedBinding.swipeRefreshLayout.isRefreshing = false
        when (response) {
            is NetworkResult.Success -> {
                // bind data to the view
                mActivityNewsFeedBinding.progressBar.visibility = View.GONE
                mActivityNewsFeedBinding.toolbarTitle.text = response.data?.title
                val feedList = response.data?.rows as ArrayList<Rows>
                feedList.removeIf { value -> value.title.isNullOrEmpty() }
                response.data.rows.let { mNewsFeedAdapter.setData(it) }
            }
            is NetworkResult.Error -> {
                // hide the loader and show snackbar error
                mActivityNewsFeedBinding.progressBar.visibility = View.GONE
                Snackbar.make(feedList, response.message.toString(), LENGTH_LONG).show()
            }
            is NetworkResult.Loading -> {
                // show a progress bar and snackbar
                mActivityNewsFeedBinding.progressBar.visibility = View.VISIBLE
                Snackbar.make(feedList, "Loading...", LENGTH_SHORT).show()
            }
        }
    }
}