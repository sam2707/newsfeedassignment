package com.newfeed.newsfeedapplication.main.model

data class Feed(var title: String, var rows: List<Rows>)

data class Rows(var title: String, var description: String, var imageHref: String)
